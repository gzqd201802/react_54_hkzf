import { Carousel } from "antd-mobile";
import React from 'react';
// import TestCarousel from "../../demo/TestCarousel";
// 导入局部样式
import styles from "./index.module.scss";
// console.log(styles);    // {index_swiper: "Home_index_swiper__3TxJo"}

// 导入图片静态资源
import img01 from '../../assets/images/nav-1.png';
import img02 from '../../assets/images/nav-2.png';
import img03 from '../../assets/images/nav-3.png';
import img04 from '../../assets/images/nav-4.png';
// 导入抽离的搜索框组件
import Search from "../../components/Search/index";

class Home extends React.Component {
  state = {
    // 轮播图
    swiperData: [],
    // 租房小组
    groupsData: [],
    // 新闻资讯
    newsData: [],
    // 首页入口数据 - 静态结构自己手写数据用于列表渲染
    entryData: [
      { id: 1, imgSrc: img01, href: '', text: '整租' },
      { id: 2, imgSrc: img02, href: '', text: '合租' },
      { id: 3, imgSrc: img03, href: '', text: '地图找房' },
      { id: 4, imgSrc: img04, href: '', text: '去出租' },
    ]
  };
  // 生命周期函数 - 组件挂载完毕
  componentDidMount() {
    this.getSwiper();
    this.getGroups();
    this.getNews();
  }
  // axios 获取数据
  getSwiper = () => {
    this.$axios.get('/home/swiper')
      .then(swiperData => {
        // console.log(res.data.body);
        // console.log(swiperData);
        this.setState({ swiperData });
      });
  };
  getGroups = () => {
    this.$axios.get('/home/groups')
      .then(groupsData => {
        // console.log('groupsData', groupsData);
        this.setState({ groupsData });
      });
  };
  getNews = () => {
    this.$axios.get('/home/news')
      .then(newsData => {
        // console.log('newsData', newsData);
        this.setState({ newsData });
      });
  };
  // 生命周期函数 - 渲染页面
  render() {
    return (
      <>
        {/* 应用局部样式类名 */}
        {/* 1.0 轮播图分区 */}
        <div className={styles.swiper}>
          <Search />
          {
            !!this.state.swiperData.length &&
            <Carousel autoplay infinite >
              {this.state.swiperData.map(item => (
                <a key={item.id} className={styles.swiper_height}>
                  <img
                    className={styles.swiper_height}
                    src={this.$axios.baseURL + item.imgSrc}
                    alt=""
                  />
                </a>
              ))}
            </Carousel>
          }
        </div>
        {/* 2.0 首页入口 - 静态布局 */}
        <div className={styles.entry}>
          {
            this.state.entryData.map(item =>
              <a className={styles.entry_item} href={item.href} key={item.id}>
                <img className={styles.entry_item_img} src={item.imgSrc} alt="" />
                <span className={styles.entry_item_text}>{item.text}</span>
              </a>
            )
          }
        </div>
        {/* 3.0 首页租房小组 */}
        <div className={styles.groups}>
          <div className={styles.groups_title}>
            <h3>租房小组</h3>
            <a href="#">更多</a>
          </div>
          <div className={styles.groups_main}>
            {this.state.groupsData.map(item =>
              <a href="#" key={item.id}>
                <img src={this.$axios.baseURL + item.imgSrc} alt="" />
                <h4>{item.title}</h4>
                <p>{item.desc}</p>
              </a>
            )}
          </div>
        </div>
        {/* 4.0 最新资讯 */}
        <div className={styles.news}>
          <h3 className={styles.news_title}>最新资讯</h3>
          <div className={styles.news_main}>
            {this.state.newsData.map(item =>
              <div className={styles.news_item} key={item.id}>
                <img className={styles.news_item_img} src={this.$axios.baseURL + item.imgSrc} />
                <div className={styles.news_item_info}>
                  <h4>{item.title}</h4>
                  <p><span>{item.from}</span><span>{item.date}</span></p>
                </div>
              </div>
            )}
          </div>
        </div>
      </>
    );
  }
}
export default Home;