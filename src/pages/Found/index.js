import { Icon, NavBar, PickerView } from "antd-mobile";
import React from 'react';
import { List } from "react-virtualized";
import Search from "../../components/Search";
import styles from './index.module.scss';
import HouseItem from "../../components/HouseItem";
import TestPickerView from "../../demo/TestPickerView";


// 静态不变的数据可以定义到组件外
const filterTabs = [
  { id: 1, title: '区域', cols: 3 },
  { id: 2, title: '方式', cols: 1 },
  { id: 3, title: '租金', cols: 1 },
  { id: 4, title: '筛选', cols: 0 },
];

class Found extends React.Component {
  state = {
    // 房源列表
    houseList: [],
    // 活跃状态的 tabs 索引，由于默认没有被选中，所以默认值不设置为 0
    activeIndex: -1,
    // PickerView 数据准备
    pickerViewData: [],
    // 准备一个数组，根据 activeIndex 的值获取对应 PickerView 的数据
    filterData: [[], [], [], []],
  };
  componentDidMount() {
    // 获取当前城市下的房源筛选数据（如北京）
    this.getConditionData();
    // 获取房源列表
    // this.getHouseList();
  }
  getHouseListParams = () => {
    // params.cityId = 'AREA|88cff55c-aaa4-e2e0';
    const { filterData } = this.state;
    // 处理索引为0 PickerView 的参数
    const key = filterData[0][0];
    // 获取 value 值的3种情况：
    //    1. 第一列选择 区域 或 地铁  area  subway
    //    2. 第二列选择 具体值(value) 或 不限(null)
    //    3. 第三列选择 具体值(value) 或 不限(null)
    // 如果第三项不为 null，我们就用第三项作为 value 值，否则用第二项作为值
    // console.log('第三项数据', filterData[0][2], typeof filterData[0][2]);
    const value = ['null', undefined].includes(filterData[0][2]) ? filterData[0][1] : filterData[0][2];
    console.log(key, value);
    // 请求参数处理
    let params = {
      [key]: value,
      rentType: filterData[1][0],
      price: filterData[2][0],
      more: filterData[3].join(',')
    };
    // console.log('params', params);
    return params;
  };
  // 获取房源列表
  getHouseList = async () => {
    const params = this.getHouseListParams();
    params.cityId = 'AREA|88cff55c-aaa4-e2e0';
    const { list: houseList } = await this.$axios.get('/houses', { params });
    this.setState({ houseList });
  };
  // 获取当前城市下的房源筛选数据
  getConditionData = async () => {
    // 房源筛选数据准备
    let pickerViewData = [];
    // 解构出房源筛选数据
    const {
      area,             // 区域
      subway,           // 地铁
      rentType,         // 方式
      price,            // 租金
      roomType,         // 户型
      oriented,         // 朝向
      floor,            // 楼层
      characteristic,   // 房屋亮点
    } = await this.$axios.get('/houses/condition', { params: { id: 'AREA|88cff55c-aaa4-e2e0' } });

    // 索引0 PickerView 的数据 - 区域 和 地铁
    pickerViewData[0] = [area, subway];
    // 索引1 PickerView 的数据 - 租房方式
    pickerViewData[1] = rentType;
    // 索引2 PickerView 的数据 - 租金
    pickerViewData[2] = price;
    // 索引3 PickerView 的数据 - 更多筛选
    pickerViewData[3] = [
      { title: '户型', list: roomType },
      { title: '朝向', list: oriented },
      { title: '楼层', list: floor },
      { title: '房屋亮点', list: characteristic },
    ];

    console.log('pickerViewData', pickerViewData);
    // 数据处理完成后，更新到状态中
    this.setState({ pickerViewData });

  };
  // 虚拟化列表 - 用于渲染虚拟列表的函数
  rowRenderer = ({ key, index, style, }) => {
    // 根据索引值获取需要渲染的数据
    const item = this.state.houseList[index];
    // console.log(item);
    return (
      <div key={key} style={style}>
        {/* 调用自己封装的组件 */}
        <HouseItem {...item} />
      </div>
    );
  };
  // 当 PickerView 值改变时触发
  onPickerViewChange = (value) => {
    // console.log(value);
    const { filterData, activeIndex } = this.state;
    // 把 PickerView 获取的值直接存到 filterData 中
    filterData[activeIndex] = value;
    // 更新组件状态
    this.setState({ filterData }, () => {
      console.log(this.state);
    });
  };
  // 点击更多筛选
  onMoreFilterClick = (value) => {
    // console.log(value);
    // const { filterData } = this.state;
    // React 不可变值写法
    const filterData = [...this.state.filterData];
    // console.log(filterData === this.state.filterData);  // false，内存地址不同
    // 两种情况：
    //    1. 如果当前点击的 value 不在数组中，我们就添加
    //    2. 如果已经存在数组中，我们就删除
    const index = filterData[3].findIndex(item => item === value);
    if (index === -1) {
      filterData[3].push(value);
    } else {
      filterData[3].splice(index, 1);
    }
    // 更新状态
    this.setState({ filterData });
  };
  // 清除更多筛选
  clearFilterData = () => {
    // React 不可变值写法
    const filterData = [...this.state.filterData];
    // 清除更多筛选条件
    filterData[3] = [];
    this.setState({ filterData });
  };
  // 根据选中的索引，渲染房源筛选内容
  renderFilterContent = () => {
    const { activeIndex } = this.state;
    // console.log(activeIndex);
    switch (activeIndex) {
      // 前三个返回 PickerView 结构
      case 0:
      case 1:
      case 2:
        return <div className={styles.filter_content}>
          <div className={styles.filter_content_main}>
            <PickerView
              data={this.state.pickerViewData[activeIndex]}
              cols={filterTabs[activeIndex].cols}
              onChange={this.onPickerViewChange}
              value={this.state.filterData[activeIndex]}
            ></PickerView>
          </div>
          <div className={styles.filter_content_buttons}>
            <span className={styles.btn_cancel}>取消</span>
            <span onClick={this.getHouseList} className={styles.btn_confirm}>确定</span>
          </div>
        </div>;
      case 3:
        {/* 更多筛选 */ }
        return <div className={styles.filter_more}>
          <div className={styles.filter_more_content}>
            {/* 添加一个判断条件，如果数据还没回来，暂时不渲染右侧更多筛选 */}
            {this.state.pickerViewData[3] && this.state.pickerViewData[3].map(item =>
              <div key={item.title}>
                <h3 className={styles.filter_more_title}>{item.title}</h3>
                <div className={styles.filter_more_list}>
                  {item.list.map(item2 =>
                    <span
                      key={item2.label}
                      onClick={() => this.onMoreFilterClick(item2.value)}
                      className={this.state.filterData[3].includes(item2.value) ? styles.active : ''}
                    >{item2.label}</span>
                  )}
                </div>
              </div>)}
          </div>
          <div className={styles.filter_content_buttons}>
            <span onClick={this.clearFilterData} className={styles.btn_cancel}>清除</span>
            <span onClick={this.getHouseList} className={styles.btn_confirm}>确定</span>
          </div>
        </div>;
      default:
        // 返回空标签代表不渲染内容
        return <></>;
    }
    // 或者使用以下分支结构
    // if (activeIndex === 0 || activeIndex === 1 || activeIndex === 2) {} 
    // else if (activeIndex === 3) {}
    // else {}
  };
  render() {
    return (
      <>
        {/* 1.0 页面标题 */}
        <div className={styles.navbar}
        >
          <Icon onClick={() => { this.props.history.go(-1); }} className={styles.icon_left} color="#666" type="left"></Icon>
          <Search className={styles.search} />
        </div>
        {/* 2.0 筛选分区 */}
        <div className={styles.filter}>
          {/* 2.1 筛选分区标题部分 */}
          <div className={styles.filter_tabs}>
            {filterTabs.map((item, index) =>
              <span
                className={[styles.filter_tabs_item, this.state.activeIndex === index ? styles.active : ''].join(' ')}
                key={item.id}
                onClick={() => { this.setState({ activeIndex: index }); }}
              >
                {item.title}
                <i className="iconfont icon-arrow"></i>
              </span>
            )}
          </div>
          {/* 2.2 筛选分区内容部分 */}
          {this.renderFilterContent()}
        </div>
        {/* 3.0 房源列表 - 虚拟化列表 */}
        <div className={styles.houses}>
          <List
            width={window.screen.width}
            height={window.screen.height - 45 - 40 - 50}
            rowCount={this.state.houseList.length}   // 大行的长度
            rowHeight={104}                  // 计算大行高度
            rowRenderer={this.rowRenderer}              // 大行渲染
          />
        </div>
        {/* 4.0 遮罩层 */}
        <div
          className={[styles.mask_bg, this.state.activeIndex !== -1 ? styles.mask_show : ''].join(' ')}
          onClick={() => { this.setState({ activeIndex: -1 }); }}
        ></div>
      </>
    );
  }
}
export default Found;