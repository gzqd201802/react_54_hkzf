import React from 'react';
import { connect } from "react-redux";
// 导入头部导航组件
import { NavBar, Icon } from 'antd-mobile';
// 导入局部样式
import styles from "./index.module.scss";
import { withRouter } from "react-router-dom";
import HouseItem from "../../components/HouseItem";
// console.log(styles);

// 解构出来 window 上的全局变量
const { BMap } = window;
// 为了方便访问到地图实例，把地图实例的变量变成公共变量
let map;
// 用于地图定位的中心位置，需要拼接
let areaCenterName = '';
// 用于根据不同的数据级别展示不同的<缩放级别>和<覆盖物类名>
let levelDataIndex = 0;  // 索引用于选择哪个级别的，每次点击覆盖物，索引就累加 1
let labelZIndex = 1;
let levelData = [
  { id: 1, scale: 12, className: 'map_circle' },
  { id: 2, scale: 14, className: 'map_circle' },
  { id: 3, scale: 15, className: 'map_rect' },
];

/* 
  获取城市房源数据：
    1. 请求1：先通过城市名称换取一个城市 id
    2. 请求2：根据城市的 id 获取房源信息
    3. 根据房源信息绘制覆盖物

  获取区域的房源数据：
    1. 清除地图覆盖物
    2. 发送网络请求，传入区域的 id
    3. 根据房源信息绘制覆盖物

  设置地图中心点
  设置地图缩放级别
  设置覆盖物类名

  三级房源覆盖物点击
    1. 展示房屋列表组件
    2. 发送网络请求获取房源列表数据
    3. 渲染到组件中

    4. 覆盖物移动到视觉中心
    5. 被点击的覆盖物层级提升一级
  
  地图移动的时候，隐藏房屋列表组件
  
*/
class MapFound extends React.Component {
  // constructor() 生命周期
  state = {
    isHideList: true,
    houseList: [],
  };
  componentDidMount() {
    // console.log('MapFound的componentDidMount', this.props);
    // 绘制百度地图
    // console.log('组件的状态state', this.state);
    // console.log('JS文件的变量levelDataIndex', levelDataIndex);
    this.renderMap();
  }
  componentWillUnmount() {
    // console.log('MapFound的componentWillUnmount，组件被卸载了');
    // JS 文件的变量要在组件被卸载时手动重置一下
    areaCenterName = '';
    levelDataIndex = 0;
  }
  // shouldComponentUpdate() {
  //   console.log('shouldComponentUpdate', this.props);
  //   return true;
  // }
  // componentDidUpdate() {
  //   console.log('componentDidUpdate', this.props);
  // }
  renderMap = () => {
    // 初始化地图，传入一个 id 名称，注意名称一致
    map = new BMap.Map("container");
    map.addEventListener('dragstart', () => {
      // console.log('需要隐藏房源列表');
      this.setState({ isHideList: true });
    });
    // 缩放级别 1-19 级
    // map.centerAndZoom(this.props.cityName, 12);
    // 添加地图控件
    map.addControl(new BMap.NavigationControl());
    map.addControl(new BMap.GeolocationControl());
    // 由于比例尺官方有布局错乱问题，需要单独延时执行
    setTimeout(() => {
      map.addControl(new BMap.ScaleControl());
    }, 5000);
    // 调用获取城市数据的请求
    this.getHousesData();
  };
  // 获取房源数据
  getHousesData = async () => {
    // 城市名称字符串
    const { cityName: areaName } = this.props;
    // 拼接地区中心点位置
    areaCenterName += areaName;
    // 1. 请求1：先通过城市名称换取一个城市 id
    const { value: id } = await this.$axios.get(`/area/info?name=${areaName}`);
    // console.log('城市id', id);
    // 2. 请求2：根据城市的 id 获取房源信息
    // 获取区域列表
    const areaList = await this.$axios.get(`/area/map?id=${id}`);
    // console.log('城市房源数据', areaList);
    // 调用渲染覆盖物的函数
    this.renderOverlay(areaList);
  };
  // 渲染覆盖物的函数 - 递归函数封装
  renderOverlay = (areaList) => {
    // console.log('areaCenterName 区域中心', areaCenterName);
    // 清除地图所有覆盖物
    map.clearOverlays();
    // 设置地图中心点位置
    map.centerAndZoom(areaCenterName, levelData[levelDataIndex].scale);
    // console.log('当前的房源信息', areaList);
    // 根据新的数据绘制新的覆盖物
    areaList.forEach(item => {
      // 解构出需要的展示的数据
      const { label: areaName, value: id, count, coord } = item;
      // 根据城市房源数据绘制覆盖物
      const opts = {
        position: new BMap.Point(coord.longitude, coord.latitude),
        offset: new BMap.Size(-35, -35)
      };
      // 创建文本覆盖物，通过覆盖物内的 div 控制样式，切换类名即可切换样式
      const label = new BMap.Label(`<div class="${levelData[levelDataIndex].className}"><span>${areaName}</span><span>${count}套</span></div>`, opts);
      // 清除文本覆盖物原本的默认样式
      label.setStyle({ border: 'none', backgroundColor: 'transparent' });
      // 把覆盖物添加到地图中
      map.addOverlay(label);
      label.addEventListener('click', (e) => {
        // 0 1 
        if (levelDataIndex < 2) {
          // 每次点击拼接地区中心点位置
          areaCenterName += areaName;
          // 每次点击级别索引累加1
          levelDataIndex += 1;
          this.$axios.get(`/area/map?id=${id}`).then(areaList => {
            console.log('点击后再查看房源信息', areaList);
            // 递归：函数内部自己调用自己，不过这里传入的是新的房源数据
            this.renderOverlay(areaList);
          });
        } else {
          // console.log('到达了3级，展示房屋列表');
          this.getLevel3HourseList(id);
          this.setState({ isHideList: false });
          // 获取触摸点坐标
          const { clientX, clientY } = e.changedTouches[0];
          // 把点击的位置移动到屏幕中心的公式
          const x = window.screen.width / 2 - clientX;
          const y = window.screen.height / 2 / 2 - clientY;
          // 根据坐标移动地图
          map.panBy(x, y);
          // 层级提升一级
          label.setZIndex(++labelZIndex);

        }
      });
    });
  };

  // 获取3级房源列表
  getLevel3HourseList = async (id) => {
    const { list: houseList } = await this.$axios.get(`/houses?area=${id}`);
    console.log('houseList', houseList);
    this.setState({ houseList });
  };

  render() {
    console.log('render', this.props);
    return (
      <div className={styles.found_page}>
        {/* 1.0 使用头部导航组件 */}
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => this.props.history.go(-1)}
        >地图找房</NavBar>
        {/* 2.0 百度地图 */}
        <div className={styles.baidu_map} id="container"></div>
        {/* 3.0 房屋列表 */}
        <div className={[styles.house_list, this.state.isHideList ? '' : 'show'].join(' ')}>
          <div className={styles.house_list_title}>
            <h3>房屋列表</h3>
            <a href="#">更多房源</a>
          </div>
          <div className={styles.house_list_main}>
            {/* 直接把 item 对象解构成属性进行传值 */}
            {this.state.houseList.slice(0, 3).map(item => <HouseItem key={item.houseCode} {...item} />)}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { ...state.mapReducer };
};

// 把仓库的 城市状态 映射到组件的 props 中
export default withRouter(connect(mapStateToProps)(MapFound));