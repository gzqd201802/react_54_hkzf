import React from 'react';

class PageNotFound extends React.Component {
  // 组件加载完毕时
  componentDidMount() {
    // 通过单次定时器在 2s 后跳转回首页
    setTimeout(() => {
      this.props.history.replace('/home');
    }, 2000);
  }
  render() {
    return (
      <>
        <h1>页面没找到，2秒后自动跳转到首页</h1>
      </>
    );
  }
}
export default PageNotFound;