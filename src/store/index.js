// `src\store\index.js`
// 仓库入口文件，Redux 使用一个仓库即可
import { createStore, combineReducers, applyMiddleware } from "redux";
// redux 的插件，需要通过 应用中间件函数 applyMiddleware 启动插件
// reduxThunk 主要功能，对 redux 的 dispatch 功能增强了
// 增强前：dispatch 只能传入普通对象，
// 增强后：dispatch 不仅能传入普通对象，dispatch 还能传入一个函数，函数内部可以写异步请求等。
import reduxThunk from "redux-thunk";

// 导入仓库管理员
import cartReducer from "./reducers/cartReducer";
import userReducer from "./reducers/userReducer";
import mapReducer from "./reducers/mapReducer";
// 创建仓库并传入仓库管理员
const store = createStore(
  // combineReducers({管理员1,管理员2})  用于结合多个管理员到一起。
  combineReducers({ cartReducer, userReducer, mapReducer }),
  // 应用 reduxThunk 中间件，对 redux 的 dispatch API 增强
  applyMiddleware(reduxThunk)
);
// 导出 redux 仓库
export default store;
