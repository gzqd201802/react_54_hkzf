// 就是用来管理 action.type 的字符串的一个文件
// 这个操作看似简单，但是很有必要。
// 优点：使用变量名有语法提示，减少手打字符串导致的低级错误。

export const CART_ADD = 'CART_ADD';
export const CART_LESS = 'CART_LESS';

export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGOUT = 'USER_LOGOUT';


export const GET_CITY = 'GET_CITY';
export const CHANGE_CITY = 'CHANGE_CITY';

// 通过 export default 导出一个对象也可以
// const CART_ADD = 'CART_ADD';
// const CART_LESS = 'CART_LESS';
// export default { CART_ADD, CART_LESS };


