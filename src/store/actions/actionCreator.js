// src\store\actions\actionCreator.js
// 把 action 也封装抽离出一个文件，方便后期对仓库的所有 action 进行管理。
import { getCityByBaiduMap } from "../../utils/baiduMap";
import { USER_LOGIN, CART_ADD, GET_CITY, CHANGE_CITY } from "./actionType";



// 普通的 action 对象
export const actionObj = { type: CART_ADD };

// 通过函数创建 action 对象，Action 创建函数 就是生成 action 的函数。
export const createUserLoginAction = (username) => {
  return { type: USER_LOGIN, value: username };
};


// 异步 action，内部可以发送异步请求，获取用户信息
export const getUserInfoAction = () => {
  // 返回一个有业务的函数
  return (dispatch) => {
    // 模拟的异步请求成功，用户名在请求成功的 res 中
    setTimeout((res = '小白') => {
      dispatch({ type: USER_LOGIN, value: res });
    }, 2000);
  };
};


// 获取城市信息也使用 异步 action
export const getCityAction = () => {
  // 返回一个有业务的函数
  return (dispatch) => {
    // 调用封装的百度地区获取定位城市的方法
    getCityByBaiduMap()
      // 获取成功时
      .then(res => {
        // 回调函数内部触发 dispatch() 发送 action 修改仓库的数据
        dispatch({ type: GET_CITY, value: res.replace('市', '') });
      });
  };
};


// 改变城市数据
export const changeCityAction = (cityName) => {
  // 返回一个有业务的函数
  return { type: CHANGE_CITY, value: cityName };
};
