// `src\store\reducers\cartReducer.js`
import { CART_ADD, CART_LESS } from "../actions/actionType";

// 仓库默认数据
const defaultState = {
  cartNum: 100,
};

// 仓库管理员
const cartReducer = (state = defaultState, action) => {
  if (action.type === CART_ADD) {
    // 要求返回全新对象，更新仓库的数据
    return { cartNum: state.cartNum + 1 };
  }
  else if (action.type === CART_LESS) {
    // 要求返回全新对象，更新仓库的数据
    return { cartNum: state.cartNum - 1 };
  }
  else {
    // 如果没有 action 就直接返回状态
    return state;
  }
};

// 导出 reducer 函数
export default cartReducer;
