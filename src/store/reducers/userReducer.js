import { USER_LOGIN, USER_LOGOUT } from "../actions/actionType";

// 仓库默认数据
const defaultState = {
  userInfo: { userName: '游客' }
};

// 仓库管理员
const userReducer = (state = defaultState, action) => {
  // if (action.type === USER_LOGIN) {
  //   return { userInfo: { userName: '帅哥你好' } };
  // }
  if (action.type === USER_LOGIN) {
    return { userInfo: { userName: action.value } };
  }
  else if (action.type === USER_LOGOUT) {
    return { userInfo: { userName: '游客' } };
  }
  else {
    // 如果没有 action 就直接返回状态
    return state;
  }
};

// 导出 reducer 函数
export default userReducer;
