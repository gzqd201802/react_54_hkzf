import { Toast } from "antd-mobile";
import axios from 'axios';

// 全局的 axios 默认值：可以设置默认的 URL 地址
// axios.defaults.baseURL = 'http://157.122.54.189:9060';

// 把根路径变成变量，1. 用于创建 axios 实例，2. 用于在页面中拼接图片路径
// const baseURL = 'http://157.122.54.189:9060';
const baseURL = 'https://api-haoke-web.itheima.net';
// 创建自定义 axios 实例
const newAxios = axios.create({ baseURL });

// 准备一个计数器，用来管理什么时候关闭 Toast 提示
let ajaxCount = 0;
// 添加<请求>拦截器
newAxios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  Toast.loading('加载中...', 0);
  // 每个请求前计数器先累加
  ajaxCount++;
  // 返回请求配置，用于发送请求的
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加<响应>拦截器
newAxios.interceptors.response.use(function (response) {
  // 每个响应计数器都减少
  ajaxCount--;
  // 如果计数器为 0，才隐藏 Toast
  if (ajaxCount === 0) {
    // console.log('拦截器运行了，运行了隐藏 Toast 的代码');
    // 对响应数据做点什么
    Toast.hide();
  }
  // 返回响应结果，用于在页面中渲染的
  return response.data.body;
}, function (error) {
  // 对响应错误做点什么
  Toast.offline('网络出问题了.');
  return Promise.reject(error);
});

// 给函数添加一个路径属性，方便在页面中使用
newAxios.baseURL = baseURL;

export default newAxios;

// const myAxios = (params) => {
//   return new Promise((resolve, reject) => {
//     newAxios({ ...params }).then(res => {
//       // console.log(res.data.body);
//       resolve(res.data.body);
//     });
//   });
// };
// // 给函数添加一个路径属性
// myAxios.baseURL = baseURL;
// export default myAxios;
