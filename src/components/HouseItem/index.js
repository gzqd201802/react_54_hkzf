import React from 'react';
import styles from "./index.module.scss";
import newAxios from "../../utils/myAxios";

// 直接把传递过来的所有属性从 props 中解构出来
const HouseItem = ({ title, price, houseImg, desc, tags }) => {
  // this.$axios.baseURL   函数式组件没有this，只能在类组件中使用this
  // console.log('HouseItem的props', props);
  return <div className={styles.house_item}>
    <img src={newAxios.baseURL + houseImg} alt="" />
    <div className={styles.house_item_info}>
      <h4 className={styles.house_item_info_name}>{title}</h4>
      <div className={styles.house_item_info_desc}>{desc}</div>
      <div className={styles.house_item_info_tags}>
        {tags.map(item => <span key={item}>{item}</span>)}
      </div>
      <div className={styles.house_item_info_price}>{price}</div>
    </div>
  </div>;
};

export default HouseItem;