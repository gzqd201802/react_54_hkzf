import React from 'react';
import { connect } from "react-redux";
import styles from "./index.module.scss";
import { withRouter } from "react-router-dom";

/* 
  点击城市，跳转到城市选择页  /citylist
*/
class Search extends React.Component {
  render() {
    console.log('search组件的props', this.props);
    return <>
      {/* 搜索框组件, 接收了父组件传递过来的类名 */}
      <div className={[styles.search, this.props.className].join(' ')}>
        <div className={styles.search_main}>
          <span
            className={styles.search_city}
            onClick={() => this.props.history.push('/citylist')}
          >
            {this.props.cityName}
            <i className="iconfont icon-arrow"></i>
          </span>
          <i className={`iconfont icon-seach ${styles.icon_search}`}></i>
          <span className={styles.search_text}>请输入校区或地址</span>
        </div>
        <div
          className={styles.search_map}
          onClick={() => this.props.history.push('/map')}
        >
          <i className="iconfont icon-map"></i>
        </div>
      </div>
    </>;
  }
}

// connect(mapStateToProps,mapDispatchToProps);
const mapStateToProps = (state) => {
  // 把 mapReducer 的地图数据映射到搜索框组件中
  return { ...state.mapReducer };
};

export default withRouter(connect(mapStateToProps)(Search));