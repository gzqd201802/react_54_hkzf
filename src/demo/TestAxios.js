import React from 'react';
// import axios from 'axios';

class TestAxios extends React.Component {
  // 生命周期函数 constructor()
  state = {
    swiperData: []
  };

  // 需要改变 DOM 结构的网络请求写到这里
  componentDidMount() {
    // console.log(this.$axios);
    this.$axios
      .get('/home/swiper')
      .then(res => {
        // console.log(res);
        // console.log(res.data.body);
        this.setState({ swiperData: res.data.body }, () => {
          console.log(this.state);
        });
      });
  }

  // 生命周期函数 render
  render() {
    return (
      <>
        <h1>axios应用</h1>
      </>
    );
  }
}

export default TestAxios;