import React from 'react';
import { List } from 'react-virtualized';

// List data as an array of strings
// const list = [
//   'Brian Vaughn',
//   // And so on...
// ];

// 生成 1万条 测试用的数据，自己看看页面中到底渲染了多少个 DOM 节点
const list = [...new Array(10000).keys()].map((item, index) => `序列数组${index}`);

// key,             唯一标识
// index,           索引
// isScrolling,     布尔值，当前列表是否在滚动
// isVisible,       当前行是否可见
// style,           当前行的样式

// list.map(item=>{
//   return <div key={key} style={style}>
//     {item}
//   </div>
// });

function rowRenderer({ key, index, isScrolling, isVisible, style, }) {
  // console.log(key, style, list[index]);
  const item = list[index];
  return (
    <div key={key} style={style}>
      {/* list[index]  代表一条数据，类似之前 map 中的 item */}
      {item}
    </div>
  );
}

class TestReactVirtualized extends React.Component {

  // 创建组件 ref 关联
  ListRef = React.createRef();

  render() {
    // window.screen.width    屏幕宽度，数值类型
    // window.screen.height   屏幕高度，数值类型，减去头部高度 45 像素
    /* 
      width           列表总宽
      height          列表总高
      rowCount        总列表行数
      rowHeight       单个行高度
      rowRenderer     单个行渲染函数
    */
    return (
      <>
        <button onClick={() => { console.log('', this.ListRef.current); }}>获取组件</button>
        <List
          width={window.screen.width}
          height={window.screen.height - 45}
          rowCount={list.length}
          rowHeight={() => { return 2 * 20; }}
          rowRenderer={rowRenderer}
          ref={this.ListRef}
          scrollToIndex={100}
          scrollToAlignment="start"
        />
      </>

    );
  }
}

export default TestReactVirtualized;