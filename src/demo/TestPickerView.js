import React from 'react';
import { PickerView, WhiteSpace } from 'antd-mobile';


/* 
    {
      label: '2013',    // 展示给用户看的数据
      value: '2013',    // 真正获取的数据
      children: [ ]     // !!! 级联的子数据
    },
*/

// 没有级联的数据
const seasons = [
  [
    {
      label: '2013',
      value: '2013',
    },
    {
      label: '2014',
      value: '2014',
    },
  ],
  [
    {
      label: '春',
      value: '春',
    },
    {
      label: '夏',
      value: '夏',
    },
  ],
];

// 没有级联的数据
const season = [
  {
    label: '春',
    value: '春',
  },
  {
    label: '夏',
    value: '夏',
  },
];

// 用于级联的数据 - 特点：有 children 属性
const province = [
  {
    label: '北京',
    value: '01',
    children: [
      {
        label: '东城区',
        value: '01-1',
      },
      {
        label: '西城区',
        value: '01-2',
      },
      {
        label: '崇文区',
        value: '01-3',
      },
      {
        label: '宣武区',
        value: '01-4',
      },
    ],
  },
  {
    label: '浙江',
    value: '02',
    children: [
      {
        label: '杭州',
        value: '02-1',
        children: [
          {
            label: '西湖区',
            value: '02-1-1',
          },
          {
            label: '上城区',
            value: '02-1-2',
          },
          {
            label: '江干区',
            value: '02-1-3',
          },
          {
            label: '下城区',
            value: '02-1-4',
          },
        ],
      },
      {
        label: '宁波',
        value: '02-2',
        children: [
          {
            label: 'xx区',
            value: '02-2-1',
          },
          {
            label: 'yy区',
            value: '02-2-2',
          },
        ],
      },
      {
        label: '温州',
        value: '02-3',
      },
      {
        label: '嘉兴',
        value: '02-4',
      },
      {
        label: '湖州',
        value: '02-5',
      },
      {
        label: '绍兴',
        value: '02-6',
      },
    ],
  },
];

class TestPickerView extends React.Component {
  state = {
    value: null,
    value2: null,
  };
  onChange = (value) => {
    console.log(value);
    this.setState({ value, });    // ["2013", "夏"]
  };
  // onScrollChange = (value) => {
  //   console.log(value);
  // };
  onChange2 = (value2) => {
    console.log(value2);          // ["夏"] 只有一项也是数组格式
    this.setState({ value2 });
  };
  render() {
    return (
      <div>
        {/* 1. 两列数据 - 没有级联 */}
        <PickerView
          onChange={this.onChange}    // 绑定 onChange 获取 value 值
          value={this.state.value}    // value 值根据 state 发送变化 
          data={seasons}              // 展示的数据
          cascade={false}             // 不需要级联
        />
        <WhiteSpace /><WhiteSpace />
        {/* 2. 一列数据 - 没有级联 */}
        <PickerView
          data={season}
          cascade={false}
          onChange={this.onChange2}
          value={this.state.value2}
        />
        <WhiteSpace /><WhiteSpace />
        {/* 3.0 三列数据 - 开启了级联 */}
        <PickerView
          cascade={true}
          data={province}
          value={['01', '01-1', '']}
        />
      </div>
    );
  }
}

export default TestPickerView;