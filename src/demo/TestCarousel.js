import React from "react";
// WingBlank 两侧留白组件，这里用不上，删除掉
import { Carousel } from 'antd-mobile';
import styles from './test.module.scss';


/* 
  轮播图组件使用
  1. 把 state 的 data 变成空数组，图片资源没有，控制台报错
  2. data 数据为空的时候不能渲染轮播图组件，否则轮播图组件不能自动播放
*/


class TestCarousel extends React.Component {
  // 组件状态
  state = {
    data: [],
    imgHeight: 176,
  };
  // 组件挂载完成
  componentDidMount() {
    // simulate img loading
    setTimeout(() => {
      this.setState({
        data: ['AiyWuByWklrrUDlFignR', 'TekJlZRVCjLFexlOCuWn', 'IJOtIlfsYdTyaDTRVrLI'],
      });
    }, 1000);
  }
  // 渲染的结构
  render() {
    return (
      <>
        {/* 添加条件渲染，data 数据有长度，才渲染轮播图组件 */}
        {/* 
          条件渲染：
            写法1：this.state.data.length === 0 
            写法2：Boolean(this.state.data.length)
            写法3：!!this.state.data.length
        */}
        {
          !!this.state.data.length &&
          <Carousel
            autoplay
            infinite
          >
            {this.state.data.map(val => (
              <a key={val} className={styles.swiper_height}>
                <img className={styles.swiper_height} src={`https://zos.alipayobjects.com/rmsportal/${val}.png`} />
              </a>
            ))}
          </Carousel>
        }
      </>
    );
  }
}

export default TestCarousel;

// ReactDOM.render(<App />, mountNode);