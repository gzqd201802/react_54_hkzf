import React from 'react';
import { NavBar, Icon } from 'antd-mobile';

const TestNavBar = () => {
  return <div>
    <NavBar
      mode="light"
      icon={<Icon type="left" />}
      onLeftClick={() => console.log('你点击了左边，我们要实现返回上一页')}
    >标题</NavBar>

    <NavBar
      mode="dark"
      leftContent="Back"
      rightContent={[
        <Icon key="0" type="search" style={{ marginRight: '16px' }} />,
        <Icon key="1" type="ellipsis" />,
      ]}
    >地图找房</NavBar>
  </div>;
};

export default TestNavBar;