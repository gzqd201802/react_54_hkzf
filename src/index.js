import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './styles/global.scss';
import myAxios from "./utils/myAxios";
// 导入仓库
import store from "./store";
// 导入仓库数据提供商
import { Provider } from "react-redux";

React.Component.prototype.$axios = myAxios;

ReactDOM.render(
  // 把 App 组件用 Provider 包起来
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
