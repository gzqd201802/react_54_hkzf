import React from 'react';
import { HashRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import CitySelect from "./pages/CitySelect";
import Index from "./pages/Index";
import MapFound from "./pages/MapFound";
import PageNotFound from "./pages/PageNotFound";
// 导入 connect 函数，用于把 dispatch 映射到组件 props 中
import { connect } from "react-redux";
// 导入 action 创建函数
import { getCityAction } from "./store/actions/actionCreator";
// 1 引入 ant 组件
// import { Button } from "antd-mobile";
// import TabBarExample from "./demo/TestTabBar";
// import TestAxios from "./demo/TestAxios";


class App extends React.Component {
  // 生命周期函数 - 组件挂载完毕时
  componentDidMount() {
    // 触发获取城市定位的 dispatch
    this.props.getCity();
  }
  render() {
    console.log('App组件的render', this.props);
    return (
      <>
        <Router>
          <Switch>
            {/* <Route exact path="/" component={Index}></Route> */}
            {/* 如果用户进来的是根路径，重定向到 /home  */}
            <Redirect exact path="/" to="/home"></Redirect>
            <Route path="/home" component={Index}></Route>
            <Route path="/map">
              {this.props.cityName !== '定位中' && <MapFound />}
            </Route>
            <Route path="/citylist" component={CitySelect}></Route>
            <Route component={PageNotFound}></Route>
          </Switch>
        </Router>
      </>
    );
  }
}

// 把 state 映射到 props 中的函数
const mapStateToProps = (state) => {
  return { ...state.mapReducer };
};

// 把 dispatch 映射到 props 中的函数
const mapDispatchToProps = (dispatch) => {
  return {
    // 获取城市定位的 dispatch
    getCity: () => {
      // action 创建函数内部获取定位的城市，最终有 dispatch 发送到仓库，修改仓库数据
      dispatch(getCityAction());
    }
  };
};

// connect 把 dispatch 映射到组件 props 中
export default connect(mapStateToProps, mapDispatchToProps)(App);