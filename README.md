# React 脚手架

## 安装命令

### 安装脚手架

#### 方式1

```shell
npm install -g create-react-app

create-react-app my-app
```

#### 方式2

```shell
npx create-react-app my-app
```

## 运行项目

```shell
npm run start
或
yarn start
```

### 安装 `package.json` 的依赖

拉取项目的时候，`node_modules` 都是在自己电脑中下载安装的。

```shell
npm install
或
yarn
```

`npm` 和 `yarn` 只能二选一，否则 `node_modules` 会被反复删除和卸载。

一开始用 `npm` 下载，以后都用 `npm`

`yarn` 同理，不过 node 默认没有 yarn，需要通过 `npm` 全局安装 `yarn` 后才能使用。

```shell
npm install -g yarn
```

## 已安装依赖包

### 安装列表

```json
{
    "react": "^16.13.1",
    "react-scripts": "3.4.3",
    "react-dom": "^16.13.1",
    "react-router-dom": "^5.2.0",
    "redux": "^4.0.5",
    "react-redux": "^7.2.1",
    "node-sass": "^4.14.1",
    "sass-loader": "^10.0.3"
}
```

### 安装 sass 

```shell
npm install node-sass sass-loader
或
yarn add node-sass sass-loader
```

### 安装 react-router-dom

```shell
npm install react-router-dom
或
yarn add react-router-dom
```

### 安装 redux  react-redux  redux-thunk

```shell
npm install redux react-redux  redux-thunk
或
yarn add redux react-redux redux-thunk
```



